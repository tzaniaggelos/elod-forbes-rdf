package ontology;

import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.ResourceFactory;

/**
 * @author A. Tzanis
 */
public class Ontology {

	/** prefixes **/
	public static final String instancePrefix;
	public static final String eLodPrefix;
	public static final String foafPrefix;
	public static final String skosPrefix;
	public static final String orgPrefix;
	public static final String goodRelationsPrefix;	
	public static final String regOrgPrefix;
	public static final String elodGeoPrefix;
	
	/** Classes **/
	// eLod
	public static final Resource rankResource; 
	public static final Resource fundamentalResource;
	public static final Resource conceptsResource;
	public static final Resource currencyResource;
	//public static final Resource relatesResource;
	// SKOS
	public static final Resource forbesSectorResource;
	public static final Resource forbesIndustryResource;
	// GR
	public static final Resource businessEntityResource;
	// FOAF
	public static final Resource agentResource;
	public static final Resource organizationResource;
	// org
	public static final Resource orgOrganizationResource;
	// rov
	public static final Resource registeredOrganizationResource;
	//psnetGeo
	public static final Resource countryResource;
		
	/** Object Properties **/
	// eLod
	public static final Property isRegisteredAt;	// Company is registered at country
	public static final Property hasRank; 			// Company hasRank rank
	public static final Property hasFundamentals; 	// Company hasFundamentals funtamentals
	public static final Property sector; 			// Company sector Sector
	public static final Property industry;			// Company industry Industry
	public static final Property hasCurrency;		// Fundamental hasCurrency USD
	
	/** Data Properties **/
	// eLod
	public static final Property forbesWebPage;		// Company Property
	public static final Property rank;				// Rank property
	public static final Property referenceYear;		// Fundamental property
	public static final Property marketValue;		// Fundamental property
	public static final Property sales;				// Fundamental property
	public static final Property profits;			// Fundamental property
	public static final Property assets;			// Fundamental property
	public static final Property isin; 				// Company property
	public static final Property relatesTo; 		// Company property
	// gr
	public static final Property name;				// Company property
	//SKOS
	public static final Property prefLabel;
	
	static {
		/** prefixes **/
		instancePrefix = "http://linkedeconomy.org/resource/";
		eLodPrefix = "http://linkedeconomy.org/ontology#";
		foafPrefix = "http://xmlns.com/foaf/0.1/";
		skosPrefix = "http://www.w3.org/2004/02/skos/core#";
		orgPrefix = "http://www.w3.org/ns/org#";
		goodRelationsPrefix = "http://purl.org/goodrelations/v1#";
		regOrgPrefix = "http://www.w3.org/ns/regorg#";
		elodGeoPrefix = "http://linkedeconomy.org/geoOntology#";
		
		/** Resources **/
		/*// eLod
		rankResource = ResourceFactory.createResource(eLodPrefix + "Rank"); 
		fundamentalResource = ResourceFactory.createResource(eLodPrefix + "Fundamental");
		conceptsResource = ResourceFactory.createResource(eLodPrefix + "Concepts");
		*/
		
		// eLod
		//companyResource = ResourceFactory.createResource(instancePrefix + "Company");
		rankResource = ResourceFactory.createResource(eLodPrefix + "Rank"); 
		fundamentalResource = ResourceFactory.createResource(eLodPrefix + "Fundamental");
		currencyResource = ResourceFactory.createResource(eLodPrefix + "Currency");
		forbesSectorResource = ResourceFactory.createResource(eLodPrefix + "ForbesSector");
		forbesIndustryResource = ResourceFactory.createResource(eLodPrefix + "ForbesIndustry");
		//relatesResource = ResourceFactory.createResource(eLodPrefix + "Relates");
		// FOAF
		agentResource = ResourceFactory.createResource(foafPrefix + "Agent");
		organizationResource = ResourceFactory.createResource(foafPrefix + "Organization");
		// SKOS
		conceptsResource = ResourceFactory.createResource(skosPrefix + "Concept");
		// GR
		businessEntityResource = ResourceFactory.createResource(goodRelationsPrefix + "BusinessEntity");
		// RegOrg
		registeredOrganizationResource = ResourceFactory.createResource(regOrgPrefix + "RegisteredOrganization");
		// org
		orgOrganizationResource = ResourceFactory.createResource(orgPrefix + "Organization");
		//psnetGeo
		countryResource = ResourceFactory.createResource(elodGeoPrefix + "Country");
				
		/** Object Properties **/
		// eLod
		hasCurrency = ResourceFactory.createProperty(eLodPrefix + "hasCurrency");
		isRegisteredAt = ResourceFactory.createProperty(eLodPrefix + "isRegisteredAt");
		hasRank = ResourceFactory.createProperty(eLodPrefix + "hasRank");
		hasFundamentals = ResourceFactory.createProperty(eLodPrefix + "hasFundamentals");
		sector = ResourceFactory.createProperty(eLodPrefix + "sector");
		industry = ResourceFactory.createProperty(eLodPrefix + "industry");
		
		/** Data Properties **/
		// eLod
		forbesWebPage = ResourceFactory.createProperty(eLodPrefix + "forbesWebPage");
		rank = ResourceFactory.createProperty(eLodPrefix + "rank");
		referenceYear = ResourceFactory.createProperty(eLodPrefix + "referenceYear");
		marketValue = ResourceFactory.createProperty(eLodPrefix + "marketValue");
		sales = ResourceFactory.createProperty(eLodPrefix + "sales");
		profits = ResourceFactory.createProperty(eLodPrefix + "profits");
		assets = ResourceFactory.createProperty(eLodPrefix + "assets");
		isin = ResourceFactory.createProperty(eLodPrefix + "isin");
		relatesTo = ResourceFactory.createProperty(eLodPrefix + "relatesTo");
		// gr
		name = ResourceFactory.createProperty(goodRelationsPrefix + "name");
		//SKOS
		prefLabel = ResourceFactory.createProperty(skosPrefix + "prefLabel");
					
	}

}