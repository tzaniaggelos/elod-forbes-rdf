 package actions;


import ontology.OntologyInitialization;
//import utils.HelperMethods;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Iterator;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;

import com.hp.hpl.jena.rdf.model.Model;
import objects.ExcelObject;
//import utils.HelperMethods;

/**
 * @author A. Tzani
 */
@SuppressWarnings("unused")
public class Main {
	
	
	/**
     * Method to initiate the process
     */
	public static void main(String[] args) {
		
		boolean createConcepts = true;
		
		//HelperMethods hm = new HelperMethods();
		ReadExcel excelMethods = new ReadExcel();
		RdfActions rdfActions = new RdfActions();
		ConceptCreator concepts = new ConceptCreator();
		OntologyInitialization ontInit = new OntologyInitialization();
		



		Model model = rdfActions.remoteOrLocalModel(false);
		
		//Perform the basic initialization on the model
		ontInit.setPrefixes(model);
		ontInit.createHierarchies(model);
		//ontInit.addIndividualsToModel(model);
		
		//add the Concepts
		if (createConcepts) {
			concepts.addSectorToModel(model);
			concepts.addForbesIndustryToModel(model);
		}
		
		//Read excel content
		try { 
			FileInputStream fileInputStream = new FileInputStream("Forbes Global 2000-2014.xls");
			HSSFWorkbook workbook = new HSSFWorkbook(fileInputStream);
			HSSFSheet worksheet = workbook.getSheet("Forbes Global 2000 - 2014");
			
		
		Iterator<?> rows = worksheet.rowIterator();
		int rownum=0;	//Row counter
		
		while( rows.hasNext() ){ 
				
			if( rownum<1 ) {			//Just pass the first row
				   rownum++;
				   HSSFRow row = (HSSFRow) rows.next();
				   continue; 		
				   }
			
			else {
			HSSFRow row = (HSSFRow) rows.next(); 
			System.out.println("\n"); 
			
			//Create excelObject for every excel row
			ExcelObject excelObject = excelMethods.main(row);
			rdfActions.createRdfFromExcel(excelObject, model);
			rownum++;
			}
		}
		
		fileInputStream.close();
		
		}
		catch ( IOException ex ) { 
			ex.printStackTrace(); 
			}
		
		
		/* store the model at the end of each folder */
		rdfActions.writeModel(model);
		model.close();
				
		model.close();
		
		System.out.println("\nFinished!");
		
		
		}
		
	}