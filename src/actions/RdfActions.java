package actions;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import objects.ExcelObject;
import ontology.Ontology;
import ontology.OntologyInitialization;
import utils.CountryOriented;
import utils.HelperMethods;
import virtuoso.jena.driver.VirtGraph;
import virtuoso.jena.driver.VirtModel;
import com.hp.hpl.jena.datatypes.xsd.XSDDatatype;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Resource;


/**
 * @author A. Tzanis
 */

public class RdfActions {
	
	/**
     * Fetch all the details from the provided Excel object, 
     * create the RDF triples and add them to the existing model.
     * 
     * @param excelObject a Excel file object
     * @param Model the model to add the triples
     */
	public void createRdfFromExcel(ExcelObject excelObject, Model model) {
		
		
		HelperMethods hm = new HelperMethods();
		CountryOriented co = new CountryOriented();		
		
		/** rankResource **/
    	Resource rankResource = null;
    	if (excelObject.getRank() == 2032) { //special unresolved case
    		if (excelObject.getCompanyIsin() != null) {
    		rankResource = model.createResource(Ontology.instancePrefix + "Rank/" + excelObject.getCompanyIsin().replaceAll("\\s+","") + "/2015", Ontology.rankResource);
    		}
    		else{
    			rankResource = model.createResource(Ontology.instancePrefix + "Rank/" + excelObject.getCompanyName().replaceAll("\\s+","") + "/2015", Ontology.rankResource);
        	}
    		rankResource.addLiteral(Ontology.rank, model.createTypedLiteral("32", XSDDatatype.XSDint));
		} else if (excelObject.getRank() != ' ' && (excelObject.getRank() != 0) ) {										
			if (excelObject.getCompanyIsin() != null) {  //if we have find the company isin code
	    		rankResource = model.createResource(Ontology.instancePrefix + "Rank/" + excelObject.getCompanyIsin().replaceAll("\\s+","") + "/2015", Ontology.rankResource);
	    	}
	    	else{	//if we have not find company isin code
	    			rankResource = model.createResource(Ontology.instancePrefix + "Rank/" + excelObject.getCompanyName().replaceAll("\\s+","") + "/2015", Ontology.rankResource);
	        }
	    		rankResource.addLiteral(Ontology.rank, model.createTypedLiteral(excelObject.getRank(), XSDDatatype.XSDint));
		}
		
		//referenceYear
		rankResource.addLiteral(Ontology.referenceYear, model.createTypedLiteral("2015", XSDDatatype.XSDgYear));
		
		/** fundamentalResource **/
		Resource fundamentalResource = null;
		if (excelObject.getCompanyIsin() != null) {		//if we have find the company isin code
			fundamentalResource = model.createResource(Ontology.instancePrefix + "Funtamentals/" + excelObject.getCompanyIsin().replaceAll("\\s+","") + "/2015", Ontology.fundamentalResource);
		}
		else{		//if we have not find company isin code
			fundamentalResource = model.createResource(Ontology.instancePrefix + "Funtamentals/" + excelObject.getCompanyName().replaceAll("\\s+","") + "/2015", Ontology.fundamentalResource);
		}
			// Fundamental marketValue
		if (excelObject.getMarketValue() != ' ' && excelObject.getMarketValue() != 0 ) {										
			fundamentalResource.addLiteral(Ontology.marketValue, model.createTypedLiteral((excelObject.getMarketValue()*1000000000), XSDDatatype.XSDfloat));		
		    }
		// Fundamental sales
		if (excelObject.getSales() != ' ' && excelObject.getSales() != 0 ) {										
			fundamentalResource.addLiteral(Ontology.sales, model.createTypedLiteral((excelObject.getSales()*1000000000), XSDDatatype.XSDfloat));		
		    }
		// Fundamental profits
		if (excelObject.getProfits() != ' ' && excelObject.getProfits() != 0 ) {										
			fundamentalResource.addLiteral(Ontology.profits, model.createTypedLiteral((excelObject.getProfits()*1000000000), XSDDatatype.XSDfloat));		
			}
		// Fundamental assets
		if (excelObject.getAssets() != ' ' && excelObject.getAssets() != 0 ) {										
			fundamentalResource.addLiteral(Ontology.assets, model.createTypedLiteral((excelObject.getAssets()*1000000000), XSDDatatype.XSDfloat));		
			}
		// Fundamental referenceYear
		fundamentalResource.addLiteral(Ontology.referenceYear, model.createTypedLiteral("2015", XSDDatatype.XSDgYear));
		// Fundamental hasCurrency
		Resource currencyResource = model.createResource(Ontology.instancePrefix + "Currency/USD", Ontology.currencyResource);
		/** configure prefLabel **/
		currencyResource.addProperty(Ontology.prefLabel, "United States Dollars", "en");
		currencyResource.addProperty(Ontology.prefLabel, "Δολάρια Ηνωμένων Πολιτειών Αμερικής", "el");
		fundamentalResource.addProperty(Ontology.hasCurrency, currencyResource);
		
		/** ForbesSectorResource **/
		Resource forbesSectorResource = null;
		if (excelObject.getSector() != null) {
			String[] sectorIndividualUri = hm.findForbesSectorIndividual(excelObject.getSector());
			forbesSectorResource = model.createResource(sectorIndividualUri[0], Ontology.forbesSectorResource);
		}
		
		/** ForbesIndustryResource **/
		Resource forbesIndustryResource = null;
		if (excelObject.getIndustry() != null) {
			String[] industryIndividualUri = hm.findForbesIndustryIndividual(excelObject.getIndustry());
			forbesIndustryResource = model.createResource(industryIndividualUri[0], Ontology.forbesIndustryResource);
		}						
		
		/** agentResource **/
		Resource agentResource = null;
		if (excelObject.getCompanyIsin() != null) {			//if we have find company isin code							
			agentResource = model.createResource(Ontology.instancePrefix + "Organization/" + excelObject.getCompanyIsin().replaceAll("\\s+",""), Ontology.organizationResource);
		model.createResource(Ontology.instancePrefix + "Organization/" + excelObject.getCompanyIsin().replaceAll("\\s+",""), Ontology.businessEntityResource);
		model.createResource(Ontology.instancePrefix + "Organization/" + excelObject.getCompanyIsin().replaceAll("\\s+",""), Ontology.orgOrganizationResource);
		model.createResource(Ontology.instancePrefix + "Organization/" + excelObject.getCompanyIsin().replaceAll("\\s+",""), Ontology.registeredOrganizationResource);
		}
		else if (excelObject.getCompanyName() != null) {	//if we have not find company isin code									
				agentResource = model.createResource(Ontology.instancePrefix + "Organization/" + excelObject.getCompanyName().replaceAll("\\s+",""), Ontology.organizationResource);
				model.createResource(Ontology.instancePrefix + "Organization/" + excelObject.getCompanyName().replaceAll("\\s+",""), Ontology.businessEntityResource);
				model.createResource(Ontology.instancePrefix + "Organization/" + excelObject.getCompanyName().replaceAll("\\s+",""), Ontology.orgOrganizationResource);
				model.createResource(Ontology.instancePrefix + "Organization/" + excelObject.getCompanyName().replaceAll("\\s+",""), Ontology.registeredOrganizationResource);
		}
		// Company name
		if (excelObject.getCompanyName() != null) {										
			agentResource.addLiteral(Ontology.name, model.createTypedLiteral(excelObject.getCompanyName(), XSDDatatype.XSDstring));		
    	}
		// Company isin
		if (excelObject.getCompanyIsin() != null) {										
			agentResource.addLiteral(Ontology.isin, model.createTypedLiteral(excelObject.getCompanyIsin().replaceAll("\\s+",""), XSDDatatype.XSDstring));		
    	}
		// Company Web Page
    	if (excelObject.getWebPage() != null) {
    		agentResource.addLiteral(Ontology.forbesWebPage, excelObject.getWebPage());
    	}
    	// Company isRegisteredAt
    	if ( (excelObject.getCountry() != null) && (!excelObject.getCountry().equalsIgnoreCase("null")) ) {
    		String[] isoCode = co.findCountryAbbreviation(excelObject.getCountry());
			if (isoCode[0] != null) {
				Resource countryResource = model.createResource(Ontology.instancePrefix + "Organization/" + isoCode[0], Ontology.countryResource);
				/** configure prefLabel **/
				countryResource.addProperty(Ontology.prefLabel, isoCode[1], "en");
				countryResource.addProperty(Ontology.prefLabel, isoCode[2], "el");
	        	agentResource.addProperty(Ontology.isRegisteredAt, countryResource);
			} else {
				Resource countryResource = model.createResource(Ontology.instancePrefix + "Organization/" + excelObject.getCountry().replaceAll("\\s+",""), Ontology.countryResource);
				agentResource.addProperty(Ontology.isRegisteredAt, countryResource);
			}
    	}
    	// Company hasRank
    	if ( (excelObject.getRank() != ' ') && (excelObject.getRank() != 0) ) {
    		agentResource.addProperty(Ontology.hasRank, rankResource);
			}
    	// Company hasFundamentals
    	agentResource.addProperty(Ontology.hasFundamentals, fundamentalResource);
		
    	// Company sector
    	if (excelObject.getSector() != null) {
			agentResource.addProperty(Ontology.sector, forbesSectorResource);
		}
    	// Company industry
    	if (excelObject.getIndustry() != null) {
			agentResource.addProperty(Ontology.industry, forbesIndustryResource);
		}
    	// Company relatesTo
    	if ( (excelObject.getMatchedUri() != null) ) {
    		int matchedCounter = excelObject.getMatchedUri().size();
    		for (int i=0; i<matchedCounter; i++){
    		agentResource.addProperty(Ontology.relatesTo, model.getResource(excelObject.getMatchedUri().get(i).toString()));
    	}
    }	
}
	
	/**
     * Fetch the existing model depending whether it is located remotely or locally.
     * 
     * @param boolean is the model located remotely or locally?
     * @return Model the new model
     */
	public Model remoteOrLocalModel (boolean isRemote) {
		
		OntologyInitialization ontInit = new OntologyInitialization();
		
		Model remoteModel = ModelFactory.createDefaultModel();
		
		if (isRemote) {
			String graphName = "http://linkedeconomy.org/Eprocurement";
			String connectionString = "jdbc:virtuoso://83.212.86.155:1111/autoReconnect=true/charset=UTF-8/log_enable=2";
			VirtGraph graph = new VirtGraph (graphName, connectionString, "razis", "m@kisr@zis");
			System.out.println("\nConnected to remote Eprocurement graph!\n");
	    	remoteModel = new VirtModel(graph);
		} else {
			try {
				InputStream is = new FileInputStream(ReadExcel.filePathOutput + "/" + ReadExcel.rdfName);
				remoteModel.read(is,null);
				is.close();
			} catch (Exception e) { //empty file
			}
		}
		
		ontInit.setPrefixes(remoteModel);
		
		return remoteModel;
		
	}
	
	/**
     * Store the Model.
     * 
     * @param Model the model
     */
	public void writeModel(Model model) {
		
		try {
			System.out.println("\nSaving Model...");
			FileOutputStream fos = new FileOutputStream(ReadExcel.filePathOutput + "/" + ReadExcel.rdfName);
			model.write(fos, "RDF/XML-ABBREV", ReadExcel.filePathOutput + "/" + ReadExcel.rdfName);
			fos.close();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		
	}
	
}