package actions;

import java.util.ArrayList;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.ss.usermodel.Row;

import objects.ExcelObject;


/**
 * @author A. Tzani
 */

@SuppressWarnings("unused")
public class ReadExcel {
	
	public static final String rdfName = "ForbesIsin_11_2_2016.rdf";
	//public static final String filePathExcel = "C:/Users/Aggelos/workspace/Forbes/Forbes Global 2000 - 2014.xls";
	public static final String filePathOutput = "C:/Users/Aggelos/workspace/Forbes/";
	
	
	@SuppressWarnings("deprecation")
	public ExcelObject main(HSSFRow row) {
		
		ArrayList<String> matchedUris = new ArrayList<String>();			// list with the matched URIs
		
		String companyName = null;
		String companyIsin = null;
		String sector = null;
		String industry = null;
		String continent = null;
		String country = null;
		double marketValue = 0;
		double sales = 0;
		double profits = 0;
		double assets = 0;
		int rank = 0;
		String webPage = null;
		String matchedUri = null;
		
		ExcelObject excelObject = null;
		
				
			for (int i=0; i<=12; i++)	{
			
				HSSFCell cell = (HSSFCell) row.getCell((short) i);
				
				if(cell != null){
				
				if (i==0)	{
					companyName=cell.getStringCellValue();
					System.out.print("name = "+companyName+"\t");
				}

				if (i==1)	{
					companyIsin=cell.getStringCellValue().replaceAll("\\s+","");
					System.out.print("isin = "+companyIsin+"\t");
				}

				else if (i==2)	{
					sector=cell.getStringCellValue();
					System.out.print("sector = "+sector+"\t");
				}

				else if (i==3)	{
					industry=cell.getStringCellValue();
					System.out.print("industry = "+industry+"\t");
				}

				else if (i==4)	{
					continent=cell.getStringCellValue();
					System.out.print("continent = "+continent+"\t");
				}

				else if (i==5)	{
					country=cell.getStringCellValue();
					System.out.print("country = "+country+"\t");
				}

				else if (i==6)	{
					marketValue=cell.getNumericCellValue();
					System.out.print("marketValue = "+marketValue+"\t");
				}

				else if (i==7)	{
					sales=cell.getNumericCellValue();
					System.out.print("sales = "+sales+"\t");
				}

				else if (i==8)	{
					profits=cell.getNumericCellValue();
					System.out.print("profits = "+profits+"\t");
				}

				else if (i==9)	{
					assets=cell.getNumericCellValue();
					System.out.print("assets = "+assets+"\t");
				}

				else if (i==10)	{
					rank=(int) cell.getNumericCellValue();
					System.out.print("rank = "+rank+"\t");
				}

				else if (i==11)	{
					webPage=cell.getStringCellValue();
					System.out.print("page = "+webPage+"\t");
				}
				
				else if (i==12)	{
					while (cell != null && !cell.equals(' ')){
						matchedUri=cell.getStringCellValue().replaceAll("\"","");
						if(matchedUri.startsWith("organization-")){
							matchedUri=matchedUri.substring(25).replaceAll("-","/");
							matchedUri="http://linkedeconomy.org/resource/Organization/"+matchedUri.replaceAll("//","/");
						}
						else if (matchedUri.startsWith("person-")){
							matchedUri=matchedUri.substring(19).replaceAll("-","/");
							matchedUri="http://linkedeconomy.org/resource/Person/ "+matchedUri.replaceAll("//","/");
						}
						matchedUris.add(i-12,matchedUri);
						System.out.print("machedUri = "+matchedUris.get(i-12)+"\t");
						cell = row.getCell((short) i++);
					}
				}
				
			  }	
			}
			
			
		//create object type of excel
		excelObject = new ExcelObject(companyName,companyIsin,sector, industry, continent, country, marketValue, 
				sales, profits, assets, rank, webPage, matchedUris);
		
				
		return excelObject;
		
		
		
	} 
	
}