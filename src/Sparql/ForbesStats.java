package Sparql;

import virtuoso.jena.driver.VirtGraph;
import virtuoso.jena.driver.VirtuosoQueryExecution;
import virtuoso.jena.driver.VirtuosoQueryExecutionFactory;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;

import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.rdf.model.Literal;
import com.hp.hpl.jena.vocabulary.ResultSet;


public class ForbesStats {

	public static void main(String[] args) throws IOException {
		
		
		/* Forbes Graph */
		VirtGraph graphForbes = new VirtGraph (QueryConfiguration.queryGraphForbes, QueryConfiguration.connectionString, 
											  QueryConfiguration.username, QueryConfiguration.password);
		System.out.println("Connected to Forbes Graph!");
		
		BufferedWriter bufferedWriter = null;						//Creation and declaration of bufferdWriter as Null
		String filePath = "C:/Users/Aggelos/workspace/Forbes/CompaniesStats.csv";				//Output file path

		try {
		      
		    bufferedWriter = new BufferedWriter(new FileWriter(filePath));				//Creation of bufferedWriter object
		
		    String queryOrgs = "PREFIX elod: <http://linkedeconomy.org/ontology#>"+
					"PREFIX gr: <http://purl.org/goodrelations/v1#>"+
					"SELECT ?name ?isin (COUNT(DISTINCT ?expLine) AS ?paymentsCount)"+
					" (SUM(xsd:decimal(?amount)) AS ?paymentsAmount)"+
					"WHERE {"+
					"?company elod:relatesTo ?relates ;"+
					     	"elod:isin ?isin ;"+
					     	"gr:name ?name ."+
					"?expenseApproval elod:hasExpenditureLine ?expLine ."+
					"?expLine elod:amount ?ups ;"+
					     	"elod:seller ?relates ."+
					"?ups gr:hasCurrencyValue ?amount ."+
					"}"+
					"GROUP BY ?name ?isin"+
					"ORDER BY DESC (?paymentsAmount)"+
					"LIMIT 10";


				VirtuosoQueryExecution vqeOrgs = VirtuosoQueryExecutionFactory.create(queryOrgs, graphForbes);		
				ResultSet resultsOrgs = (ResultSet) vqeOrgs.execSelect();
				
				if (((com.hp.hpl.jena.query.ResultSet) resultsOrgs).hasNext()) {
					QuerySolution result = ((com.hp.hpl.jena.query.ResultSet) resultsOrgs).nextSolution();
					Literal name = result.getLiteral("name");
					Literal isin = result.getLiteral("isin");
					Literal paymentsCount = result.getLiteral("paymentsCount");
					Literal paymentsAmount = result.getLiteral("paymentsAmount");
					
					//resultsLine= name + "," + isin + "," + paymentsCount + "," + paymentsAmount;
					
					bufferedWriter.write(name + "," + isin + "," + paymentsCount + "," + paymentsAmount);					//Writes the results to the file
					bufferedWriter.write("\n");
				}
				
				vqeOrgs.close();
				
		}catch (FileNotFoundException e1) {
				e1.printStackTrace();
				}
		
		finally {
			
			//Closes the BufferedWriter
			try {
				if (bufferedWriter != null) {
					bufferedWriter.flush();
					bufferedWriter.close();
				}
				
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		
	}
		
}