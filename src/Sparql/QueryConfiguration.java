package Sparql;
/**
 * @author A. Tzanis
 */
public class QueryConfiguration {
	
	public static final String queryGraphForbes = "http://linkedeconomy.org/Forbes";
	
	public static final String connectionString = "jdbc:virtuoso://83.212.86.158:1111/autoReconnect=true/charset=UTF-8/log_enable=2";
	
	public static final String username = "razis";
	public static final String password = "m@kisr@zis";
	
	//public static final String dateFilterPrevious = "2013";
	//public static final String dateFilterCurrent = "2014";
	
	//public static final String jsonFilepath = "C:/Users/Makis/Desktop/eprocur/JSON/";//"/home/makis/JSON/eprocur/"; "/home/makis/JSON/eprocur/couchDb/";
	//public static final String csvFilepath = "C:/Users/Aggelos/workspace/E_Procurement/CSV/"; //"/home/makis/CSV/";
	//public static final String csvFilepath = "C:/Users/Makis/Desktop/eprocur/CSV/"; //"/home/makis/CSV/";
	
	public static final boolean couchDbUsage = false;
}