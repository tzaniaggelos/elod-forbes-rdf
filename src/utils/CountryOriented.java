package utils;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author A. Tzanis
 */
public class CountryOriented {
	
	
	/**
     * Find the the official abbreviation of the country.
     * 
     * @param String the country from the CSV file
     * @return String the official abbreviation of the country
     */
	@SuppressWarnings("null")
	public String[] findCountryAbbreviation(String country) {
		
		String[] abbreviation = null;
		
		if ( country.contains("Netherlands") ) {
			abbreviation =new String[]{ "NL","Netherlands","��������"};
		} else if (country.contains("Germany")) {
			abbreviation =new String[]{"DE","Germany","��������"};
		} else if (country.contains("Austria")) {
			abbreviation =new String[]{"AT","Austria","�������"};
		} else if ( country.contains("United States") ) {
			abbreviation =new String[]{"US","United States","�������� ��������� ��������"};
		} else if (country.contains("France")) {
			abbreviation =new String[]{"FR","France","������"};
		} else if (country.contains("Italy")) {
			abbreviation =new String[]{"IT","Italy","������"};
		} else if (country.contains("Canada")) {
			abbreviation =new String[]{"CA","Canada","�������"};
		} else if (country.contains("China")) {
			abbreviation =new String[]{"CN","China","����"};
		} else if (country.contains("United Kingdom")) {
			abbreviation =new String[]{"GB","United Kingdom","������� ��������"};
		} else if (country.contains("Turkey")) {
			abbreviation =new String[]{"TR","Turkey","�������"};
		} else if (country.contains("Japan")) {
			abbreviation =new String[]{"JP","Japan","�������"};
		} else if (country.contains("United Arab Emirates")) {
			abbreviation =new String[]{"AE","United Arab Emirates","������� ������� �������"};
		} else if (country.contains("Switzerland")) {
			abbreviation =new String[]{"CH","Switzerland","�������"};
		} else if (country.contains("Spain")) {
			abbreviation =new String[]{"ES","Spain","�������"};
		} else if (country.contains("Belgium")) {
			abbreviation =new String[]{"BE","Belgium","������"};
		} else if (country.contains("Indonesia")) {
			abbreviation =new String[]{"ID","Indonesia","���������"};
		} else if (country.contains("Finland")) {
			abbreviation =new String[]{"FI","Finland","���������"};
		} else if (country.contains("Denmark")) {
			abbreviation =new String[]{"DK","Denmark","�����"};
		} else if (country.contains("European Community")) {
			abbreviation =new String[]{"EU","European Community","��������� ���������"};
		} else if (country.contains("Australia")) {
			abbreviation =new String[]{"AU","Australia","���������"};
		} else if (country.contains("Bahrain")) {
			abbreviation =new String[]{"BH","Bahrain","��������"};
		} else if (country.contains("Brazil")) {
			abbreviation =new String[]{"BR","Brazil","��������"};
		}else if (country.contains("Chile")) {
			abbreviation =new String[]{"CL","Chile","����"};
		} else if (country.contains("Colombia")) {
			abbreviation =new String[]{"CO","Colombia","��������"};
		} else if (country.contains("Czech Republic")) {
			abbreviation =new String[]{"CZ","Czech Republic","������� ����������"};
		} else if (country.contains("Egypt")) {
			abbreviation =new String[]{"EG","Egypt","��������"};
		} else if (country.contains("Cayman Islands")) {
			abbreviation =new String[]{"KY","Cayman Islands","����� ������"};						
		} else if (country.contains("Togo")) {
			abbreviation =new String[]{"��","Togo","������"};
		} else if (country.contains("Hong Kong")) {
			abbreviation =new String[]{"HK","Hong Kong","����� �����"};
		} else if (country.contains("Hungary")) {
			abbreviation =new String[]{"HU","Hungary","��������"};
		} else if (country.contains("Ireland")) {
			abbreviation =new String[]{"IE","Ireland","��������"};
		} else if (country.contains("Israel")) {
			abbreviation =new String[]{"IL","Israel","������"};
		} else if (country.contains("India")) {
			abbreviation =new String[]{"IN","India","�����"};
		} else if (country.contains("Jordan")) {
			abbreviation =new String[]{"JO","Jordan","��������"};
		} else if (country.contains("South Korea")) {
			abbreviation =new String[]{"KR","South Korea","����� �����"};
		} else if (country.contains("Kuwait")) {
			abbreviation =new String[]{"KW","Kuwait","�������"};
		} else if (country.contains("Kazakhstan")) {
			abbreviation =new String[]{"KZ","Kazakhstan","���������"};
		} else if (country.contains("Lebanon")) {
			abbreviation =new String[]{"LB","Lebanon","������"};
		} else if (country.contains("Greece")) {
			abbreviation =new String[]{"EL","Greece","������"};
		} else if (country.contains("Luxembourg")) {
			abbreviation =new String[]{"LU","Luxembourg","������������"};
		} else if (country.contains("Mauritius")) {
			abbreviation =new String[]{"MU","Mauritius","���������"};
		} else if (country.contains("Mexico")) {
			abbreviation =new String[]{"MX","Mexico","������"};
		} else if (country.contains("Malaysia")) {
			abbreviation =new String[]{"MY","Malaysia","��������"};
		} else if (country.contains("Nigeria")) {
			abbreviation =new String[]{"NG","Nigeria","�������"};
		} else if (country.contains("Norway")) {
			abbreviation =new String[]{"NO","Norway","��������"};
		} else if (country.contains("Oman")) {
			abbreviation =new String[]{"OM","Oman","����"};
		} else if (country.contains("Peru")) {
			abbreviation =new String[]{"PE","Peru","�����"};
		} else if (country.contains("Philippines")) {
			abbreviation =new String[]{"PH","Philippines","����������"};
		} else if (country.contains("Pakistan")) {
			abbreviation =new String[]{"PK","Pakistan","��������"};
		} else if (country.contains("Poland")) {
			abbreviation =new String[]{"PL","Poland","�������"};
		} else if (country.contains("Bermuda")) {
			abbreviation =new String[]{"BM","Bermuda","��������"};
		} else if (country.contains("Puerto Rico")) {
			abbreviation =new String[]{"PR","Puerto Rico","������� ����"};						
		} else if (country.contains("Portugal")) {
			abbreviation =new String[]{"PT","Portugal","����������"};
		} else if (country.contains("Qatar")) {
			abbreviation =new String[]{"QA","Qatar","�����"};
		} else if (country.contains("Russia")) {
			abbreviation =new String[]{"RU","Russia","�����"};
		} else if (country.contains("Saudi Arabia")) {
			abbreviation =new String[]{"SA","Saudi Arabia","�������� ������"};
		} else if (country.contains("Sweden")) {
			abbreviation =new String[]{"SE","Sweden","�������"};
		} else if (country.contains("Singapore")) {
			abbreviation =new String[]{"SG","Singapore","����������"};
		} else if (country.contains("Thailand")) {
			abbreviation =new String[]{"TH","Thailand","��������"};
		} else if (country.contains("Taiwan")) {
			abbreviation =new String[]{"TW","Taiwan","������"};
		} else if (country.contains("Venezuela")) {
			abbreviation =new String[]{"VE","Venezuela","����������"};
		} else if (country.contains("Vietnam")) {
			abbreviation =new String[]{"VN","Vietnam","�������"};
		} else if (country.contains("Morocco")) {
			abbreviation =new String[]{"MA","Morocco","������"};						
		} else if (country.contains("South Africa")) {
			abbreviation =new String[]{"ZA","South Africa","����� ������"};
		} else {
			writeUnknownCountry(abbreviation[0]);
		}
		
		return abbreviation;
	}
	
	/**
     * Write in a file the unknown country read from a CSV file.
     * 
     * @param String the unknown country
     */
	public void writeUnknownCountry(String country) {
		
		try {
		    PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("countries.txt", true)));
		    out.println(country);
		    out.close();
		} catch (IOException e) {
		    e.printStackTrace();
		}
		
	}
	
	
}