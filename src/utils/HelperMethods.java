package utils;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import ontology.Ontology;

public class HelperMethods {

	
	/**
     * Find the corresponding SKOS Concept, its weight, the English name and the Greek name of the provided type of the criterion.
     * 
     * @param String the type of the criterion
     * @return String the corresponding URI of the SKOS Concept, its weight,English name and Greek name
     * of the provided type of the forbes Sector
     */
	public String[] findForbesSectorIndividual(String forbesSector) {
		
		String[] forbesSectorDtls = null;
		
		if (forbesSector.equalsIgnoreCase("Financials")) {
			forbesSectorDtls = new String[]{Ontology.instancePrefix + "ForbesSector/" + "Financials", "Financials", "����������"};
		} else if (forbesSector.equalsIgnoreCase("Energy")) {
			forbesSectorDtls = new String[]{Ontology.instancePrefix + "ForbesSector/" + "Energy", "Energy", "��������"};
		} else if (forbesSector.equalsIgnoreCase("Industrials")) {
			forbesSectorDtls = new String[]{Ontology.instancePrefix + "ForbesSector/" + "Industrials", "Industrials", "�����������"};
		} else if (forbesSector.equalsIgnoreCase("Consumer Discretionary")) {
			forbesSectorDtls = new String[]{Ontology.instancePrefix + "ForbesSector/" + "ConsumerDiscretionary", "Consumer Discretionary", "������������ �����"};
		} else if (forbesSector.equalsIgnoreCase("Information Technology")) {
			forbesSectorDtls = new String[]{Ontology.instancePrefix + "ForbesSector/" + "InformationTechnology", "Information Technology", "���������� ��� �����������"};
		} else if (forbesSector.equalsIgnoreCase("Telecommunication Services")) {
			forbesSectorDtls = new String[]{Ontology.instancePrefix + "ForbesSector/" + "TelecommunicationServices", "Telecommunication Services", "��������� ���������������"};
		} else if (forbesSector.equalsIgnoreCase("Consumer Staples")) {
			forbesSectorDtls = new String[]{Ontology.instancePrefix + "ForbesSector/" + "ConsumerStaples", "Consumer Straples", "  �����������"};		/////////
		} else if (forbesSector.equalsIgnoreCase("Health Care")) {
			forbesSectorDtls = new String[]{Ontology.instancePrefix + "ForbesSector/" + "HealthCare", "Health Care","�������� ������"};
		} else if (forbesSector.equalsIgnoreCase("Materials")) {
			forbesSectorDtls = new String[]{Ontology.instancePrefix + "ForbesSector/" + "Materials", "Materials", "�����"};
		} else if (forbesSector.equalsIgnoreCase("Utilities")) {
			forbesSectorDtls = new String[]{Ontology.instancePrefix + "ForbesSector/" + "Utilities", "Utilities", "���������"};		//////////
		}else { 
			forbesSectorDtls = new String[]{Ontology.instancePrefix + "ForbesSector/" + "Unknown", "", ""};
			writeUnknownMetadata("forbesSector", forbesSector);
		}
		
		return forbesSectorDtls;
	}
	
	
public String[] findForbesIndustryIndividual(String forbesIndustry) {
		
		String[] forbesIndustryDtls = null;
		
		if (forbesIndustry.equalsIgnoreCase("Major Banks")) {
			forbesIndustryDtls = new String[]{Ontology.instancePrefix + "ForbesIndustry/" + "MajorBanks", "Major Banks", "������� ��������"};
		} else if (forbesIndustry.equalsIgnoreCase("Regional Banks")) {
			forbesIndustryDtls = new String[]{Ontology.instancePrefix + "ForbesIndustry/" + "RegionalBanks", "Regional Banks", "������������� ��������"};
		} else if (forbesIndustry.equalsIgnoreCase("Investment Services")) {
			forbesIndustryDtls = new String[]{Ontology.instancePrefix + "ForbesIndustry/" + "InvestmentServices", "Investment Services", "����������� ���������"};
		} else if (forbesIndustry.equalsIgnoreCase("Oil & Gas Operations")) {
			forbesIndustryDtls = new String[]{Ontology.instancePrefix + "ForbesIndustry/" + "Oil&GasOperations", "Oil & GasOperations", "����������� ���������� & ������"};
		} else if (forbesIndustry.equalsIgnoreCase("Conglomerates")) {
			forbesIndustryDtls = new String[]{Ontology.instancePrefix + "ForbesIndustry/" + "Conglomerates", "Conglomerates", "������ ���������� ��������������"};
		} else if (forbesIndustry.equalsIgnoreCase("Auto & Truck Manufacturers")) {
			forbesIndustryDtls = new String[]{Ontology.instancePrefix + "ForbesIndustry/" + "Auto&TruckManufacturers", "Auto & Truck Manufacturers", "������������� ����������� & �������� "};
		} else if (forbesIndustry.equalsIgnoreCase("Computer Hardware")) {
			forbesIndustryDtls = new String[]{Ontology.instancePrefix + "ForbesIndustry/" + "ComputerHardware", "Computer Hardware", "����� �����������"};
		} else if (forbesIndustry.equalsIgnoreCase("Discount Stores")) {
			forbesIndustryDtls = new String[]{Ontology.instancePrefix + "ForbesIndustry/" + "DiscountStores", "Discount Stores", "��������� ����������� "};
		} else if (forbesIndustry.equalsIgnoreCase("Semiconductors")) {
			forbesIndustryDtls = new String[]{Ontology.instancePrefix + "ForbesIndustry/" + "Semiconductors", "Semiconductors", "���������"};
		} else if (forbesIndustry.equalsIgnoreCase("Telecommunications Services")) {
			forbesIndustryDtls = new String[]{Ontology.instancePrefix + "ForbesIndustry/" + "TelecommunicationsServices", "Telecommunications Services", "��������� ���������������"};
		} else if (forbesIndustry.equalsIgnoreCase("Diversified Insurance")) {
			forbesIndustryDtls = new String[]{Ontology.instancePrefix + "ForbesIndustry/" + "DiversifiedInsurance", "Diversified Insurance", "���������������� ���������"};		///////////////////
		} else if (forbesIndustry.equalsIgnoreCase("Software & Programming")) {
			forbesIndustryDtls = new String[]{Ontology.instancePrefix + "ForbesIndustry/" + "Software&Programming", "Software & Programming", "��������� & ���������������"};
		} else if (forbesIndustry.equalsIgnoreCase("Computer Services")) {
			forbesIndustryDtls = new String[]{Ontology.instancePrefix + "ForbesIndustry/" + "ComputerServices", "Computer Services", "��������� ������������ �����������"};
		} else if (forbesIndustry.equalsIgnoreCase("Food Processing")) {
			forbesIndustryDtls = new String[]{Ontology.instancePrefix + "ForbesIndustry/" + "FoodProcessing", "Food Processing", "����������� ��������"};
		} else if (forbesIndustry.equalsIgnoreCase("Household-Personal Care")) {
			forbesIndustryDtls = new String[]{Ontology.instancePrefix + "ForbesIndustry/" + "Household-PersonalCare", "Household-Personal Care", "���� ��������-���������� ���������"};
		} else if (forbesIndustry.equalsIgnoreCase("Medical Equipment & Supplies")) {
			forbesIndustryDtls = new String[]{Ontology.instancePrefix + "ForbesIndustry/" + "MedicalEquipment&Supplies", "Medical Equipment & Supplies", "������� ����"};
		} else if (forbesIndustry.equalsIgnoreCase("Diversified Metals & Mining")) {
			forbesIndustryDtls = new String[]{Ontology.instancePrefix + "ForbesIndustry/" + "DiversifiedMetals&Mining", "Diversified Metals & Mining", "��������������� ������� & �������"};	//////////////////////////
		} else if (forbesIndustry.equalsIgnoreCase("Pharmaceuticals")) {
			forbesIndustryDtls = new String[]{Ontology.instancePrefix + "ForbesIndustry/" + "Pharmaceuticals", "Pharmaceuticals", "������������"};
		} else if (forbesIndustry.equalsIgnoreCase("Electric Utilities")) {
			forbesIndustryDtls = new String[]{Ontology.instancePrefix + "ForbesIndustry/" + "ElectricUtilities", "Electric Utilities", "��������� ���������"};	////////////////////////////
		} else if (forbesIndustry.equalsIgnoreCase("Broadcasting & Cable")) {
			forbesIndustryDtls = new String[]{Ontology.instancePrefix + "ForbesIndustry/" + "Broadcasting&Cable", "Broadcasting & Cable", "�������� & �������"};		//////////////
		} else if (forbesIndustry.equalsIgnoreCase("Beverages")) {
			forbesIndustryDtls = new String[]{Ontology.instancePrefix + "ForbesIndustry/" + "Beverages", "Beverages", "����"};
		} else if (forbesIndustry.equalsIgnoreCase("Life & Health Insurance")) {
			forbesIndustryDtls = new String[]{Ontology.instancePrefix + "ForbesIndustry/" + "Life&HealthInsurance", "Life & Health Insurance", "��������� ���� & ������"};
		} else if (forbesIndustry.equalsIgnoreCase("Diversified Chemicals")) {
			forbesIndustryDtls = new String[]{Ontology.instancePrefix + "ForbesIndustry/" + "DiversifiedChemicals", "Diversified Chemicals", "��������������� ������"};		////////////////////
		} else if (forbesIndustry.equalsIgnoreCase("Communications Equipment")) {
			forbesIndustryDtls = new String[]{Ontology.instancePrefix + "ForbesIndustry/" + "CommunicationsEquipment", "Communications Equipment", "���������� ������������"};
		} else if (forbesIndustry.equalsIgnoreCase("Aerospace & Defense")) {
			forbesIndustryDtls = new String[]{Ontology.instancePrefix + "ForbesIndustry/" + "Aerospace&Defense", "Aerospace & Defense", "�������������� & �����"};
		} else if (forbesIndustry.equalsIgnoreCase("Managed Health Care")) {
			forbesIndustryDtls = new String[]{Ontology.instancePrefix + "ForbesIndustry/" + "ManagedHealthCare", "Managed Health Care", "���������� ��������� ������"};
		} else if (forbesIndustry.equalsIgnoreCase("Drug Retail")) {
			forbesIndustryDtls = new String[]{Ontology.instancePrefix + "ForbesIndustry/" + "DrugRetail", "Drug Retail", "������� ��������"};	//////////////////
		} else if (forbesIndustry.equalsIgnoreCase("Consumer Financial Services")) {
			forbesIndustryDtls = new String[]{Ontology.instancePrefix + "ForbesIndustry/" + "ConsumerFinancialServices", "Consumer Financial Services", "������������������ ��������� �����������"};
		} else if (forbesIndustry.equalsIgnoreCase("Oil Services & Equipment")) {
			forbesIndustryDtls = new String[]{Ontology.instancePrefix + "ForbesIndustry/" + "OilServices&Equipment", "Oil Services & Equipment", "��������� ���������� & ����������"};
		} else if (forbesIndustry.equalsIgnoreCase("Heavy Equipment")) {
			forbesIndustryDtls = new String[]{Ontology.instancePrefix + "ForbesIndustry/" + "HeavyEquipment", "Heavy Equipment", "����� ����������"};
		} else if (forbesIndustry.equalsIgnoreCase("Trading Companies")) {
			forbesIndustryDtls = new String[]{Ontology.instancePrefix + "ForbesIndustry/" + "TradingCompanies", "Trading Companies", "��������� ���������"};
		} else if (forbesIndustry.equalsIgnoreCase("Home Improvement Retail")) {
			forbesIndustryDtls = new String[]{Ontology.instancePrefix + "ForbesIndustry/" + "HomeImprovementRetail", "Home Improvement Retail", "������� ��������� �������"};	///////////
		} else if (forbesIndustry.equalsIgnoreCase("Electronics")) {
			forbesIndustryDtls = new String[]{Ontology.instancePrefix + "ForbesIndustry/" + "Electronics", "Electronics", "�����������"};
		} else if (forbesIndustry.equalsIgnoreCase("Other Transportation")) {
			forbesIndustryDtls = new String[]{Ontology.instancePrefix + "ForbesIndustry/" + "OtherTransportation", "Other Transportation", "����� ���������"};
		} else if (forbesIndustry.equalsIgnoreCase("Construction Services")) {
			forbesIndustryDtls = new String[]{Ontology.instancePrefix + "ForbesIndustry/" + "ConstructionServices", "Construction Services", "��������� ����������"};
		} else if (forbesIndustry.equalsIgnoreCase("Tobacco")) {
			forbesIndustryDtls = new String[]{Ontology.instancePrefix + "ForbesIndustry/" + "Tobacco", "Tobacco", "������"};
		} else if (forbesIndustry.equalsIgnoreCase("Healthcare Services")) {
			forbesIndustryDtls = new String[]{Ontology.instancePrefix + "ForbesIndustry/" + "HealthcareServices", "Healthcare Services", "��������� ������"};
		} else if (forbesIndustry.equalsIgnoreCase("Air Courier")) {
			forbesIndustryDtls = new String[]{Ontology.instancePrefix + "ForbesIndustry/" + "AirCourier", "Air Courier", "����������� ���������"};
		} else if (forbesIndustry.equalsIgnoreCase("Airline")) {
			forbesIndustryDtls = new String[]{Ontology.instancePrefix + "ForbesIndustry/" + "Airline", "Airline", "����������"};
		} else if (forbesIndustry.equalsIgnoreCase("Natural Gas Utilities")) {
			forbesIndustryDtls = new String[]{Ontology.instancePrefix + "ForbesIndustry/" + "NaturalGasUtilities", "Natural Gas Utilities", "��������� ������� ������"};
		} else if (forbesIndustry.equalsIgnoreCase("Biotechs")) {
			forbesIndustryDtls = new String[]{Ontology.instancePrefix + "ForbesIndustry/" + "Biotechs", "Biotechs", "�������������"};
		} else if (forbesIndustry.equalsIgnoreCase("Property & Casualty Insurance")) {
			forbesIndustryDtls = new String[]{Ontology.instancePrefix + "ForbesIndustry/" + "Property&CasualtyInsurance", "Property & Casualty Insurance", "��������� ����������� & ������"};
		} else if (forbesIndustry.equalsIgnoreCase("Railroads")) {
			forbesIndustryDtls = new String[]{Ontology.instancePrefix + "ForbesIndustry/" + "Railroads", "Railroads", "������������"};
		} else if (forbesIndustry.equalsIgnoreCase("Restaurants")) {
			forbesIndustryDtls = new String[]{Ontology.instancePrefix + "ForbesIndustry/" + "Restaurants", "Restaurants", "����������"};
		} else if (forbesIndustry.equalsIgnoreCase("Apparel-Accessories")) {
			forbesIndustryDtls = new String[]{Ontology.instancePrefix + "ForbesIndustry/" + "Apparel-Accessories", "Apparel-Accessories", "������ - ��������"};
		} else if (forbesIndustry.equalsIgnoreCase("Electrical Equipment")) {
			forbesIndustryDtls = new String[]{Ontology.instancePrefix + "ForbesIndustry/" + "ElectricalEquipment", "Electrical Equipment", "�������������� ����������"};
		} else if (forbesIndustry.equalsIgnoreCase("Auto & Truck Parts")) {
			forbesIndustryDtls = new String[]{Ontology.instancePrefix + "ForbesIndustry/" + "Auto&TruckParts", "Auto & Truck Parts", "������������ ����������� & ��������"};
		} else if (forbesIndustry.equalsIgnoreCase("Iron & Steel")) {
			forbesIndustryDtls = new String[]{Ontology.instancePrefix + "ForbesIndustry/" + "Iron&Steel", "Iron & Steel", "������� & ������"};
		} else if (forbesIndustry.equalsIgnoreCase("Food Retail")) {
			forbesIndustryDtls = new String[]{Ontology.instancePrefix + "ForbesIndustry/" + "FoodRetail", "Food Retail", "������� ������� ��������"};
		} else if (forbesIndustry.equalsIgnoreCase("Computer Storage Devices")) {
			forbesIndustryDtls = new String[]{Ontology.instancePrefix + "ForbesIndustry/" + "ComputerStorageDevices", "Computer Storage Devices", "�������� ����������� �����������"};
		} else if (forbesIndustry.equalsIgnoreCase("Business Products & Supplies")) {
			forbesIndustryDtls = new String[]{Ontology.instancePrefix + "ForbesIndustry/" + "BusinessProducts&Supplies", "Business Products & Supplies", "�������������� �������� & ����������"};
		} else if (forbesIndustry.equalsIgnoreCase("Consumer Electronics")) {
			forbesIndustryDtls = new String[]{Ontology.instancePrefix + "ForbesIndustry/" + "ConsumerElectronics", "Consumer Electronics", "����������� �����������"};
		} else if (forbesIndustry.equalsIgnoreCase("Real Estate")) {
			forbesIndustryDtls = new String[]{Ontology.instancePrefix + "ForbesIndustry/" + "RealEstate", "Real Estate", "�������"};
		} else if (forbesIndustry.equalsIgnoreCase("Internet & Catalog Retail")) {
			forbesIndustryDtls = new String[]{Ontology.instancePrefix + "ForbesIndustry/" + "Internet&CatalogRetail", "Internet & Catalog Retail", "��������� & ��������� ��������"};	////////
		} else if (forbesIndustry.equalsIgnoreCase("Construction Materials")) {
			forbesIndustryDtls = new String[]{Ontology.instancePrefix + "ForbesIndustry/" + "ConstructionMaterials", "Construction Materials", "������ �����"};
		} else if (forbesIndustry.equalsIgnoreCase("Specialized Chemicals")) {
			forbesIndustryDtls = new String[]{Ontology.instancePrefix + "ForbesIndustry/" + "SpecializedChemicals", "Specialized Chemicals", "������������� ������"};
		} else if (forbesIndustry.equalsIgnoreCase("Other Industrial Equipment")) {
			forbesIndustryDtls = new String[]{Ontology.instancePrefix + "ForbesIndustry/" + "OtherIndustrialEquipment", "Other Industrial Equipment", "����� ������������ ����������"};
		} else if (forbesIndustry.equalsIgnoreCase("Apparel-Footwear Retail")) {
			forbesIndustryDtls = new String[]{Ontology.instancePrefix + "ForbesIndustry/" + "Apparel-FootwearRetail", "Apparel/Footwear Retail", "������� ������� �������/��������"};
		} else if (forbesIndustry.equalsIgnoreCase("Advertising")) {
			forbesIndustryDtls = new String[]{Ontology.instancePrefix + "ForbesIndustry/" + "Advertising", "Advertising", "���������"};
		} else if (forbesIndustry.equalsIgnoreCase("Casinos & Gaming")) {
			forbesIndustryDtls = new String[]{Ontology.instancePrefix + "ForbesIndustry/" + "Casinos&Gaming", "Casinos & Gaming", "������ & ���������"};
		} else if (forbesIndustry.equalsIgnoreCase("Paper & Paper Products")) {
			forbesIndustryDtls = new String[]{Ontology.instancePrefix + "ForbesIndustry/" + "Paper&PaperProducts", "Paper & Paper Products", "����� & �������� ������"};
		} else if (forbesIndustry.equalsIgnoreCase("Specialty Stores")) {
			forbesIndustryDtls = new String[]{Ontology.instancePrefix + "ForbesIndustry/" + "SpecialtyStores", "Specialty Stores", "������������� �����������"};
		} else if (forbesIndustry.equalsIgnoreCase("Hotels & Motels")) {
			forbesIndustryDtls = new String[]{Ontology.instancePrefix + "ForbesIndustry/" + "Hotels&Motels", "Hotels & Motels", "���������� & �����"};
		} else if (forbesIndustry.equalsIgnoreCase("Precision Healthcare Equipment")) {
			forbesIndustryDtls = new String[]{Ontology.instancePrefix + "ForbesIndustry/" + "PrecisionHealthcareEquipment", "Precision Healthcare Equipment", "������ ��������� ������"};
		} else if (forbesIndustry.equalsIgnoreCase("Business & Personal Services")) {
			forbesIndustryDtls = new String[]{Ontology.instancePrefix + "ForbesIndustry/" + "Business&PersonalServices", "Business & Personal Services", "�������������� & ���������� ���������"};
		} else if (forbesIndustry.equalsIgnoreCase("Department Stores")) {
			forbesIndustryDtls = new String[]{Ontology.instancePrefix + "ForbesIndustry/" + "DepartmentStores", "Department Stores", "���������������"};
		} else if (forbesIndustry.equalsIgnoreCase("Insurance Brokers")) {
			forbesIndustryDtls = new String[]{Ontology.instancePrefix + "ForbesIndustry/" + "InsuranceBrokers", "Insurance Brokers", "������� ����������"};
		} else if (forbesIndustry.equalsIgnoreCase("Household Appliances")) {
			forbesIndustryDtls = new String[]{Ontology.instancePrefix + "ForbesIndustry/" + "HouseholdAppliances", "Household Appliances", "�������� ��������"};
		} else if (forbesIndustry.equalsIgnoreCase("Furniture & Fixtures")) {
			forbesIndustryDtls = new String[]{Ontology.instancePrefix + "ForbesIndustry/" + "Furniture&Fixtures", "Furniture & Fixtures", "������ & ���������"};
		} else if (forbesIndustry.equalsIgnoreCase("Diversified Utilities")) {
			forbesIndustryDtls = new String[]{Ontology.instancePrefix + "ForbesIndustry/" + "DiversifiedUtilities", "Diversified Utilities", "����������� ���������"};
		} else if (forbesIndustry.equalsIgnoreCase("Aluminum")) {
			forbesIndustryDtls = new String[]{Ontology.instancePrefix + "ForbesIndustry/" + "Aluminum", "Aluminum", "���������"};
		} else if (forbesIndustry.equalsIgnoreCase("Printing & Publishing")) {
			forbesIndustryDtls = new String[]{Ontology.instancePrefix + "ForbesIndustry/" + "Printing&Publishing", "Printing & Publishing", "���������� ��� ��������"};
		} else if (forbesIndustry.equalsIgnoreCase("Computer & Electronics Retail")) {
			forbesIndustryDtls = new String[]{Ontology.instancePrefix + "ForbesIndustry/" + "Computer&ElectronicsRetail", "Computer & Electronics Retail", "������� ������� ����������� & ������������"};
		} else if (forbesIndustry.equalsIgnoreCase("Security Systems")) {
			forbesIndustryDtls = new String[]{Ontology.instancePrefix + "ForbesIndustry/" + "SecuritySystems", "Security Systems", "��������� ���������"};
		} else if (forbesIndustry.equalsIgnoreCase("Environmental & Waste")) {
			forbesIndustryDtls = new String[]{Ontology.instancePrefix + "ForbesIndustry/" + "Environmental&Waste", "Environmental & Waste", "�������������� & ��������"};
		} else if (forbesIndustry.equalsIgnoreCase("Rental & Leasing")) {
			forbesIndustryDtls = new String[]{Ontology.instancePrefix + "ForbesIndustry/" + "Rental&Leasing", "Rental & Leasing","��������� & Leasing"};
		} else if (forbesIndustry.equalsIgnoreCase("Containers & Packaging")) {
			forbesIndustryDtls = new String[]{Ontology.instancePrefix + "ForbesIndustry/" + "Containers&Packaging", "Containers & Packaging", "��������� & ����������"};
		} else if (forbesIndustry.equalsIgnoreCase("Recreational Products")) {
			forbesIndustryDtls = new String[]{Ontology.instancePrefix + "ForbesIndustry/" + "RecreationalProducts", "Recreational Products", "���������� ��������"};
		} else if (forbesIndustry.equalsIgnoreCase("Trucking")) {
			forbesIndustryDtls = new String[]{Ontology.instancePrefix + "ForbesIndustry/" + "Trucking", "Trucking",""};	////////////////////////////
		} else if (forbesIndustry.equalsIgnoreCase("Thrifts & Mortgage Finance")) {
			forbesIndustryDtls = new String[]{Ontology.instancePrefix + "ForbesIndustry/" + "Thrifts&MortgageFinance", "Thrifts & Mortgage Finance", "�������� & ���������� ����������� �������"};	///////////
		} else { 
			forbesIndustryDtls = new String[]{Ontology.instancePrefix + "ForbesIndustry/" + "Unknown", "", ""};
			writeUnknownMetadata("forbesIndustry", forbesIndustry);
		}
		
		return forbesIndustryDtls;
	}

	/**
     * Export to a file the unknown metadata.
     * 
     * @param String the output filename
     * @param String the unknown metadata
     */
	public void writeUnknownMetadata(String fileName, String metadata) {
		
		try {
		    PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(fileName + ".txt", true)));
		    out.println(metadata);
		    out.close();
		} catch (IOException e) {
		    e.printStackTrace();
		}
		
	}
	
	
}
